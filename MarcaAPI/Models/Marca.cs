﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarcaAPI.Models
{
    public class Marca
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public Marca()
        {
        }

        public Marca(string name)
        {
            Name = name;
        }

        public Marca(int id, string name)
        {
            Id = id;
            Name = name;
        }
    }
}
