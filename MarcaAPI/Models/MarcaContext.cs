﻿using Microsoft.EntityFrameworkCore;

namespace MarcaAPI.Models
{
    public class MarcaContext : DbContext
    {
        public MarcaContext(DbContextOptions<MarcaContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Marca> MarcaItems { get; set; }
        public virtual DbSet<Carro> CarroItems { get; set; }
    }
}
