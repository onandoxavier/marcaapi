﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarcaAPI.Models
{
    public class Carro
    {
        public int Id { get; set; }
        public string Modelo { get; set; }
        public int MarcaID { get; set; }
        public virtual Marca Marca { get; set; }

        public Carro()
        {
        }
        
        public Carro(string modelo, Marca marca)
        {
            Modelo = modelo;
            MarcaID = marca.Id;
            Marca = marca;
        }

        public Carro(int id, string modelo, Marca marca)
        {
            Id = id;
            Modelo = modelo;
            MarcaID = marca.Id;
            Marca = marca;
        }

        public int Acelerar { get; set; }

    }
}
