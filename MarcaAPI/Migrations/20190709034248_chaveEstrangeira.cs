﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MarcaAPI.Migrations
{
    public partial class chaveEstrangeira : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CarroItems_MarcaItems_MarcaId",
                table: "CarroItems");

            migrationBuilder.RenameColumn(
                name: "MarcaId",
                table: "CarroItems",
                newName: "MarcaID");

            migrationBuilder.RenameIndex(
                name: "IX_CarroItems_MarcaId",
                table: "CarroItems",
                newName: "IX_CarroItems_MarcaID");

            migrationBuilder.AlterColumn<int>(
                name: "MarcaID",
                table: "CarroItems",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_CarroItems_MarcaItems_MarcaID",
                table: "CarroItems",
                column: "MarcaID",
                principalTable: "MarcaItems",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CarroItems_MarcaItems_MarcaID",
                table: "CarroItems");

            migrationBuilder.RenameColumn(
                name: "MarcaID",
                table: "CarroItems",
                newName: "MarcaId");

            migrationBuilder.RenameIndex(
                name: "IX_CarroItems_MarcaID",
                table: "CarroItems",
                newName: "IX_CarroItems_MarcaId");

            migrationBuilder.AlterColumn<int>(
                name: "MarcaId",
                table: "CarroItems",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_CarroItems_MarcaItems_MarcaId",
                table: "CarroItems",
                column: "MarcaId",
                principalTable: "MarcaItems",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
