﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MarcaAPI.Migrations
{
    public partial class CorrecaoAtributoModelo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Name",
                table: "CarroItems",
                newName: "Modelo");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Modelo",
                table: "CarroItems",
                newName: "Name");
        }
    }
}
