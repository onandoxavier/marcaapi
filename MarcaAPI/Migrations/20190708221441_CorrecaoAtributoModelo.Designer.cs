﻿// <auto-generated />
using System;
using MarcaAPI.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace MarcaAPI.Migrations
{
    [DbContext(typeof(MarcaContext))]
    [Migration("20190708221441_CorrecaoAtributoModelo")]
    partial class CorrecaoAtributoModelo
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.2.4-servicing-10062")
                .HasAnnotation("Relational:MaxIdentifierLength", 64);

            modelBuilder.Entity("MarcaAPI.Models.Carro", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("MarcaId");

                    b.Property<string>("Modelo");

                    b.HasKey("Id");

                    b.HasIndex("MarcaId");

                    b.ToTable("CarroItems");
                });

            modelBuilder.Entity("MarcaAPI.Models.Marca", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("MarcaItems");
                });

            modelBuilder.Entity("MarcaAPI.Models.Carro", b =>
                {
                    b.HasOne("MarcaAPI.Models.Marca", "Marca")
                        .WithMany()
                        .HasForeignKey("MarcaId");
                });
#pragma warning restore 612, 618
        }
    }
}
