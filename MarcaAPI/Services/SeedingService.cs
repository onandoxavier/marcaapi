﻿using MarcaAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarcaAPI.Services
{
    public class SeedingService
    {
        private readonly MarcaContext _context;

        public SeedingService(MarcaContext context)
        {
            _context = context;
        }

        public void Seed()
        {
            if (_context.MarcaItems.Any() ||  _context.CarroItems.Any())
            {
                return; //o bd has been seeded
            }
            
            Marca m1 = new Marca { Id = 1, Name = "Audi" };
            Marca m2 = new Marca { Id = 2, Name = "BMW" };
            Marca m3 = new Marca { Id = 3, Name = "Chevrolet" };
            Marca m4 = new Marca { Id = 4, Name = "Ford" };
            Marca m5 = new Marca { Id = 5, Name = "Honda" };

            Carro c1 = new Carro { Id = 1, Modelo = "A3",MarcaID = m1.Id, Marca = m1 };
            Carro c2 = new Carro { Id = 2, Modelo = "A4", MarcaID = m1.Id, Marca = m1 };
            Carro c3 = new Carro { Id = 3, Modelo = "seda", MarcaID = m2.Id, Marca = m2 };
            Carro c4 = new Carro { Id = 4, Modelo = "hatch", MarcaID = m2.Id, Marca = m2 };
            Carro c5 = new Carro { Id = 5, Modelo = "onix", MarcaID = m3.Id, Marca = m3 };
            Carro c6 = new Carro { Id = 6, Modelo = "prisma", MarcaID = m3.Id, Marca = m3 };
            Carro c7 = new Carro { Id = 7, Modelo = "montana", MarcaID = m3.Id, Marca = m3 };
            Carro c8 = new Carro { Id = 8, Modelo = "ka", MarcaID = m4.Id, Marca = m4 };
            Carro c9 = new Carro { Id = 9, Modelo = "civic", MarcaID = m5.Id, Marca = m5 };
            Carro c10 = new Carro { Id = 10, Modelo = "hrv", MarcaID = m5.Id, Marca = m5 };

            _context.MarcaItems.AddRange(m1, m2, m3, m4, m5);
            _context.CarroItems.AddRange(c1, c2, c3, c4, c5, c6, c7, c8, c9, c10);          

            _context.SaveChanges();
        }
    }
}
