﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MarcaAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace MarcaAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MarcaController : ControllerBase
    {
        private readonly MarcaContext _context;

        public MarcaController(MarcaContext context)
        {
            _context = context;            
        }

        // GET: api/Marca
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Marca>>> GetMarcas()
        {
            return await _context.MarcaItems.ToListAsync();
        }

        // GET: api/Marca/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Marca>> GetMarca(int id)
        {
            var marcaItem = await _context.MarcaItems.FindAsync(id);

            if (marcaItem == null)
            {
                return NotFound();
            }

            return marcaItem;
        }

        // POST: api/Marca
        [HttpPost]
        public async Task<ActionResult<Marca>> CriaMarca(Marca item)       
        {
            _context.MarcaItems.Add(item);
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(GetMarca), new { id = item.Id }, item);
        }

        // DELETE: api/marca/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMarca(int id)
        {
            var item = await _context.MarcaItems.FindAsync(id);

            if (item == null)
            {
                return NotFound();
            }

            _context.MarcaItems.Remove(item);
            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}