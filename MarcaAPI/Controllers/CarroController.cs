﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MarcaAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace MarcaAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CarroController : ControllerBase
    {
        private readonly MarcaContext _context;

        public CarroController(MarcaContext context)
        {
            _context = context;             
        }

        // GET: api/Carro
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Carro>>> GetCarros()
        {
            return await _context.CarroItems.Include(carro => carro.Marca).ToListAsync();
        }

        // GET: api/Carro/marca -> retorna a marca de acordo com a inicial informada como paramentro
        // letra 'a' esta como default
        [Route("marca")]
        public async Task<ActionResult<IEnumerable<Carro>>> GetCarroMarca(string initial = "a")
        {
            return await _context.CarroItems.Include(carro => carro.Marca)
                .Where(x => x.Marca.Name.Substring(0,1).ToLower().Equals(initial.ToLower())).ToListAsync();
        }       

        // GET: api/Carro/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Carro>> GetCarro(int id)
        {
            Carro carroItem = await _context.CarroItems.FindAsync(id);

            if (carroItem == null)
            {
                return NotFound();
            }

            return carroItem;
        }

        [HttpPost]
        public async Task<ActionResult<Carro>> CriaCarro(Carro item)
        {
            item.Marca = await _context.MarcaItems.FindAsync(item.MarcaID);
            _context.CarroItems.Add(item);
            await _context.SaveChangesAsync();
            return CreatedAtAction(nameof(GetCarro), new { id = item.Id }, item);
        }

        // DELETE: api/Carro/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCarro(int id)
        {
            Carro item = await _context.CarroItems.FindAsync(id);

            if (item == null)
            {
                return NotFound();
            }

            _context.CarroItems.Remove(item);
            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}